from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Complaint)

admin.site.register(Senator)

admin.site.register(Event)

admin.site.register(Club)

admin.site.register(Board)

admin.site.register(Announcement)

admin.site.register(Achievement)

admin.site.register(Minute)

admin.site.register(Panel)

admin.site.register(GirlSenator)

admin.site.register(UGSenator)

admin.site.register(PGSenator)